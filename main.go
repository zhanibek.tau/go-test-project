package main

import (
	"github.com/joho/godotenv"
	"log"
	"test/route"
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatalf("Error loading .env file: %v", err)
	}
}

func main() {
	route.Init()
}

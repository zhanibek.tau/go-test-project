package model

import (
	"time"
)

type CompanyTariffs struct {
	ID           int        `json:"id"`
	CompanyId    string     `json:"company_id"`
	TariffId     int        `json:"tariff_id"`
	DateContract string     `json:"date_contract"`
	IsDisabled   int        `json:"is_disabled"`
	IsPromo      int        `json:"is_promo"`
	DateStart    time.Time  `json:"date_start"`
	DateEnd      *time.Time `json:"date_end"`
	UserId       int        `json:"user_id"`
	CreatedAt    time.Time  `json:"created_at"`
	UpdatedAt    *time.Time `json:"updated_at"`
}
type Pagination struct {
	Limit int    `json:"limit"`
	Page  int    `json:"page"`
	Sort  string `json:"sort"`
}

func (b *CompanyTariffs) TableName() string {
	return "company_tariffs"
}

package service

import (
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"net/http"
	"test/app/model"
	"test/app/repository"
	"test/app/utils"
	"test/config/database"
	"test/config/database/gormMySql"
	logger "test/internal"
)

func GetCompanyTariffs(w http.ResponseWriter, r *http.Request) gin.H {
	log := &logger.Log{}
	var err error

	dbConfig := database.ConnectionGormToMySql(w)

	gormMySql.DB, err = gorm.Open("mysql", gormMySql.DbURL(dbConfig))
	fmt.Println(gormMySql.DB)
	if err != nil {
		fmt.Println("Status:", err)
	}
	defer gormMySql.DB.Close()

	var companyTariffs model.CompanyTariffs
	pagination := utils.GeneratePaginationFromRequest(r)

	companyTariffsList, err := repository.GetCompanyTariffs(&companyTariffs, &pagination)
	if err != nil {
		log.Error("error while getting CompanyTariffs", err.Error())
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return nil
	}
	data := gin.H{
		"data":       companyTariffsList,
		"pagination": pagination,
	}

	return data
}

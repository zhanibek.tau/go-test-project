package controller

import (
	"encoding/json"
	"net/http"
	"test/app/service"
	"test/config/database/mySql"
)

type CompanyTariffsController struct {
	db *mySql.DbConfig
}

func GetCompanyTariffs(w http.ResponseWriter, r *http.Request) {
	companyTariffs := service.GetCompanyTariffs(w, r)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(companyTariffs)
}

package repository

import (
	"context"
	"database/sql"
	"test/app/model"
	"test/config/database/gormMySql"
)

type CompanyTariffsRepo interface {
	GetCompanyTariffs(ctx context.Context) (*model.CompanyTariffs, error)
}

type CompanyTariffs struct {
	db        *sql.DB
	tableName string
}

func NewCompanyTariffs(db *sql.DB, tableName string) *CompanyTariffs {
	return &CompanyTariffs{
		db:        db,
		tableName: tableName,
	}
}

func GetCompanyTariffs(companyTariffs *model.CompanyTariffs, pagination *model.Pagination) (*[]model.CompanyTariffs, error) {
	var companyTariffsList []model.CompanyTariffs
	offset := (pagination.Page - 1) * pagination.Limit
	queryBuilder := gormMySql.DB.Limit(pagination.Limit).Offset(offset).Order(pagination.Sort)
	result := queryBuilder.Model(&model.CompanyTariffs{}).Where(companyTariffs).Find(&companyTariffsList)
	if result.Error != nil {
		msg := result.Error
		return nil, msg
	}
	return &companyTariffsList, nil
}

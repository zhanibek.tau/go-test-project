package route

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"test/app/controller"
	"test/config/env"
)

func Init() {
	router := gin.Default()

	healthRouter := router.Group("/health")
	{
		healthRouter.GET("/test/v1/alive", healthcheck)
		healthRouter.GET("/test/v1/ready", healthcheck)
	}

	router.GET("/test/v1/get-company-tariffs", func(c *gin.Context) {
		controller.GetCompanyTariffs(c.Writer, c.Request)
	})

	servPort, err := env.GetPort()
	if err != nil {
		log.Fatalf("Failed to get environment variables: %s", err)
	}

	log.Fatal(router.Run(":" + servPort))
}

func healthcheck(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"success": "true", "status": http.StatusOK})
}

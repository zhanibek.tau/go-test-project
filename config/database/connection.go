package database

import (
	"database/sql"
	"fmt"
	"net/http"
	gormMySql2 "test/config/database/gormMySql"
	"test/config/database/mySql"
	"test/config/env"
	logger "test/internal"
)

func ConnectionToDbMySql(w http.ResponseWriter) *sql.DB {
	log := &logger.Log{}
	dbConfig, err := env.GetEnvs()
	if err != nil {
		log.Error("error while load envs", err.Error())
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	}
	db, err := mySql.NewDB(dbConfig)
	if err != nil {
		log.Error("error while connecting to db ", err.Error())
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	}
	log.Info("connected to db", fmt.Sprintf("%s:%d, dbname: %s", dbConfig.Host, dbConfig.Port, dbConfig.Name))

	return db
}
func ConnectionGormToMySql(w http.ResponseWriter) *gormMySql2.DBConfig {
	log := &logger.Log{}
	dbConfig, err := env.GetEnvsForGorm()

	if err != nil {
		log.Error("error while load envs", err.Error())
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	}

	return dbConfig
}

package mySql

import (
	"database/sql"
	"fmt"
)

type DbConfig struct {
	Host           string
	User           string
	Pwd            string
	Name           string
	SchemaName     string
	MaxConnections int
	Port           int
}

func NewDB(cnf *DbConfig) (*sql.DB, error) {
	uri := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local&maxAllowedPacket=0", cnf.User, cnf.Pwd, cnf.Host, cnf.Port, cnf.Name)

	db, err := sql.Open("mysql", uri)
	if err != nil {
		return nil, err
	}

	db.SetMaxIdleConns(cnf.MaxConnections)
	db.SetMaxOpenConns(cnf.MaxConnections)

	if err := db.Ping(); err != nil {
		db.Close()
		return nil, err
	}

	return db, nil
}
